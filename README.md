# exo_excel

## Les consignes pour l'exercice:

### Ce qu'il faut faire :

1. Analyser le fichier
2. Modeliser une BDD pour recupérer les données du fichier. + Etablir les règles de normalisation des données.
3. Mettre les données du fichier et les mettres dans la bdd. En les normalisant.
4. Avec pandas faire des tables équivalentes aux TCD du fichier.
5. Faire des fonctions en python qui permettent de récupérer les tables pandas.

option:

1. Faire une api qui permet d'appeler les fonctions.
2. Faire un Dash qui reproduit le graph présent dans le fichier (avec les dorpdowns)

### Les conditions de livraison :

+ mettre le résultat de votre travail sur un repo GIT.
+ rendre le travail pour mardi 27/02/2024 à 9h00 **(modifié)**

## L'architecture du fichier excel :

Le fichier Excel `pivot-tables.xlsx` contient trois feuilles, chacune avec une structure différente :

1. **Two-dimensional Pivot Table** : Cette feuille présente une table croisée dynamique bidimensionnelle, avec des catégories de produits en lignes (Apple, Banana, Beans, Broccoli, Carrots, Mango, Orange) et des pays (ici seulement "United States" suivi d'un "Grand Total") en colonnes. Elle montre la somme des montants pour chaque combinaison de catégorie et de pays, avec une ligne et une colonne "Grand Total" pour les totaux.
2. **One-dimensional Pivot Table** : Cette feuille montre une table croisée dynamique unidimensionnelle pour la France, listant des catégories de produits (Apple, Banana, Carrots) avec le nombre d'occurrences (Count of Amount) pour chaque catégorie.
3. **Sheet1** : C'est la feuille de données brute contenant les enregistrements individuels avec les colonnes "Order ID", "Product", "Category", "Amount", "Date", et "Country". Ces données semblent être à l'origine des tables croisées dynamiques mentionnées ci-dessus.

`![L'architecture du fichier excel](ressource/Architecture_fichier.png "`L'architecture du fichier `")`.

## Modèle logique des donnée (MLD) :

les entités identifiers :

* **Produit** : Représente les différents produits vendus.
* **Catégorie** : Catégorie à laquelle appartient un produit (Fruit, Légume, etc.).
* **Vente** : Enregistrement d'une vente d'un produit spécifique.
* **Pays** : Le pays où la vente a eu lieu.

Les relations entre ces entités :

* Un **Produit** appartient à une  **Catégorie** .
* Une **Vente** est associée à un **Produit** et à un  **Pays** .

## Modèle Logique de Données (MLD) :

* **product** (**id** *int*, **product** *var(50)*, **#category** *enum,* **#country** *int*)
* **country** (**id** *int*, **country** *var(50)*)
* **order** (**id** *int*, **#produtct** *int*, **amount** *int*, **date** *date*)

## Modèle Physique de Données (MPD) :

`![Image du Modèle Physique de Données](ressource/MLD_image.png "`MLD en image `")`.

```SQL
Table product{
  id int [not null, pk, increment]
  name varchar(50) [not null]
  category enum_category [not null]
  country_id int [not null]
}

Enum enum_category {
  Vegetables
  Fruits
}

Table country{
  id int [not null, pk, increment]
  name varchar(50) [not null]
}

Table orders{
  id int [not null, pk, increment]
  product_id int [not null]
  amount int [not null]
  date date [not null]
}

Ref: product.country_id < country.name
Ref: orders.product_id < product.id
```

## Normalisation des données :

* **Produits** :
  * En minuscules.
  * Au singulier.
  * En anglais.
* **Catégories** :
  * En minuscules.
  * En anglais.
* **Pays** :
  * Première lettre de chaque mot en majuscule.
  * Pour les noms composés, remplacer les espaces par des underscores.
  * En anglais.
* **Montants** :
  * Sans symbole monétaire.
  * En entier (arrondir si c'est un décimal).
  * Sans espace.
* **Dates** :
  * Format : DD/MM/YYYY

## Exécution de l'API

Pour exécuter l'API FastAPI, utilisez la commande suivante :

<pre><div class="dark bg-gray-950 rounded-md"><div class="flex items-center relative text-token-text-secondary bg-token-main-surface-secondary px-4 py-2 text-xs font-sans justify-between rounded-t-md"><span>bash</span><span class="" data-state="closed"></span></div></div></pre>

<pre><div class="dark bg-gray-950 rounded-md"><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-bash">uvicorn main:app --reload
</code></div></div></pre>



L'API offre deux endpoints principaux :

### `/count_sales/`

Pour obtenir le nombre de ventes par produit pour un ou plusieurs pays.

- **Format de la requête** : La requête doit être envoyée au format JSON avec un champ `countries` contenant une liste des pays dont vous souhaitez compter les ventes.
  - **Exemple de payload JSON** :
    ```json
    {
      "countries": ["France", "Germany"]
    }
    ```
- **Méthode** : POST

### `/sum_amount/`

Pour obtenir la somme des montants par produit pour un ou plusieurs pays.

- **Format de la requête** : Semblable à `/count_sales/`, la requête doit inclure un champ `countries` dans le corps de la requête au format JSON, spécifiant les pays pour lesquels calculer la somme des montants.
  - **Exemple de payload JSON** :
    ```json
    {
      "countries": ["USA", "Canada"]
    }
    ```
- **Méthode** : POST
