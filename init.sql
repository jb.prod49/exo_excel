DROP DATABASE IF EXISTS exo_excel;
CREATE DATABASE exo_excel;
USE exo_excel;

CREATE TABLE `country` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL
);

CREATE TABLE `product` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category` ENUM('vegetables', 'fruit') NOT NULL,
  `country_id` int NOT NULL,
  FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
);

CREATE TABLE `orders` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `product_id` int NOT NULL,
  `amount` int NOT NULL,
  `date` date NOT NULL,
  FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
);