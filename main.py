import pandas as pd
import mysql.connector
from typing import List
from typing import Dict
from fastapi import FastAPI
from pydantic import BaseModel
from mysql.connector import Error
from config.bdd_config import DB_CONFIG, DB_CONFIG1

#--------------------------------------------------#
#------- Déclaration des fonctions pour BDD -------#
#--------------------------------------------------#
def connect_bdd(db_config):
    connect = mysql.connector.connect(**db_config)
    return connect

def close_db_connection(conn: mysql.connector.connect):
    if conn.is_connected():
        conn.close()  

def execute_query(query: str, DB_CONFIG, params=None, fetch=True):
    conn = None
    cursor = None
    try:
        conn = connect_bdd(DB_CONFIG)
        cursor = conn.cursor()
        cursor.execute(query, params or ())
        result = cursor.fetchall() if fetch else None
        return result
    except Error as e:
        print(f"Erreur lors de l'exécution de la requête : {e}")
        return None
    finally:
        if cursor is not None:
            cursor.close()
        if conn is not None:
            conn.close()
            
def execute_insert(query: str, values: tuple, DB_CONFIG)->int:
    conn = connect_bdd(DB_CONFIG)
    try:
        cursor = conn.cursor()
        cursor.execute(query, values)
        conn.commit()
        return cursor.lastrowid
    finally:
        cursor.close()
        close_db_connection(conn)

#-----------------------------------------#
#------- Déclaration des fonctions -------#
#-----------------------------------------#
def normalize_country(country:str)->str:
    return country.replace(" ", "_").title()

def normalize_product(product:str)->str:
    return product.replace(" ", "_").lower()

def normalize_category(category:str)->str:
    return category.replace(" ", "_").lower()

def count_sell_by_country(country:str, DB_CONFIG)-> list:
    normalized_country = normalize_country(country)
    tab = {normalized_country: []}
    query = """
                SELECT  
                    p.name AS ProductName, 
                    COUNT(o.id) AS NumberOfSales
                FROM 
                    orders o
                JOIN 
                    product p ON o.product_id = p.id
                JOIN 
                    country c ON p.country_id = c.id
                WHERE
                    country_id = (SELECT
                                        country.id
                                  FROM
                                        country
                                  WHERE
                                        name LIKE %s)
                GROUP BY 
                    p.name;
    """
    result = execute_query(query, DB_CONFIG, (normalized_country,))
    for row in result:
        tab[normalized_country].append({'product_name': row[0], 'count_sell': row[1]})
    return tab

def sum_amount_by_product_and_country(country: str,DB_CONFIG):
    normalized_country = normalize_country(country)
    tab = {normalized_country: []}
    query ="""
                SELECT
                    p.name AS product_name,
                    SUM(o.amount) AS sum_amount
                FROM
                    product p
                JOIN
                    orders o ON p.id = o.product_id
                JOIN
                    country c ON p.country_id = c.id
                WHERE
                    c.name LIKE %s
                GROUP BY 
                    p.name;
    """
    result = execute_query(query, DB_CONFIG, (normalized_country,))
    for row in result:
        tab[normalized_country].append({'product_name': row[0], 'sum_amount': row[1]})
    return tab
#-------------------------------------------------#
#-------- exécution du fichier init.sql ----------#
#-------------------------------------------------#
conn = connect_bdd(DB_CONFIG)
init_sql = 'init.sql'

with open(init_sql, 'r') as file:
    sql_script = file.read()

sql_statements = sql_script.split(';')

for statement in sql_statements:
     if statement.strip():
        cursor = conn.cursor()
        try:
            cursor.execute(statement)
            conn.commit()
        except mysql.connector.Error as err:
            print(f"Une erreur est survenue: {err}")
        finally:
            cursor.close()

#------------------------------------------------------------------#
#------- Importation des données du excel dans un dataframe -------#
#------------------------------------------------------------------#
df = pd.ExcelFile("ressource/pivot-tables.xlsx")
datas = pd.read_excel(df, 'Sheet1', na_values =['item1', 
                                                'item2',
                                                'item3',
                                                'item4',
                                                'item5',
                                                'item6'],
                                    parse_dates=['Date'],
                                    dtype = {"Order ID": int,
                                             "Product": str,
                                             "Category": str,
                                             "Amount": int,
                                             "Country": str 
                                            })

#---------------------------------------------------#
#------- Importation des données dans la BDD -------#
#---------------------------------------------------#

#------ ajout des pays ------#
list_country = datas['Country'].unique()

for country in list_country:
    normalized_country = normalize_country(country)
    query = "SELECT id FROM country WHERE name = %s;"
    id_country = execute_query(query, DB_CONFIG1, (normalized_country,))
    
    if not id_country:
        insert_query = "INSERT INTO country (name) VALUES (%s);"
        execute_insert(insert_query, (normalized_country,), DB_CONFIG1)

#------ ajout des produits et des ventes------#    
for index, row in datas.iterrows():
    normalized_product = normalize_product(row['Product'])
    normalized_country = normalize_country(row['Country'])
    normalized_category = normalize_category(row['Category'])
    amount = row['Amount']
    date = row['Date'].strftime('%Y-%m-%d')
    
    query = "SELECT id FROM country WHERE name = %s;"
    id_country = execute_query(query, DB_CONFIG1, (normalized_country,))[0][0]

    insert_query = "INSERT INTO product (name, category, country_id) VALUES (%s,%s,%s);"
    id_product = execute_insert(insert_query, (normalized_product, normalized_category, id_country), DB_CONFIG1)

    insert_query = "INSERT INTO orders (product_id, amount, date) VALUES (%s,%s,%s);"
    execute_insert(insert_query, (id_product, amount, date), DB_CONFIG1)

#-----------------------------------------------#
#------- appel des fonctions pour tester -------#
#-----------------------------------------------#
# country = "canada"
# print("------ le nombre de vente par produit pour le "+country+" ------")
# print(count_sell_by_country(country,DB_CONFIG1))

# print("------ la somme des montants par produit pour le "+country+" ------")
# print(sum_amount_by_product_and_country(country,DB_CONFIG1))

#---------------------------------#
#------- création de l'API -------#
#---------------------------------#
app = FastAPI()
# uvicorn main:app --reload

class CountryList(BaseModel):
    countries: List[str]

@app.post("/count_sales/")
def count_saling(country_list: CountryList):
    result = []
    for country in country_list.countries:
        temp = count_sell_by_country(country,DB_CONFIG1)
        result.append(temp)
    return result
# Format de la liste en entrée : 
# {"countries": ["France", "london", "Germany"]}


@app.post("/sum_amount/")
def count_saling(country_list: CountryList):
    result = []
    for country in country_list.countries:
        temp = sum_amount_by_product_and_country(country,DB_CONFIG1)
        result.append(temp)
    return result
# Format de la liste en entrée : 
# {"countries": ["France", "london", "Germany"]}